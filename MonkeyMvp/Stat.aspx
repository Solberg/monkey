﻿<%@ Page Language="C#" MasterPageFile="Default.Master" AutoEventWireup="true" CodeBehind="Stat.aspx.cs" Inherits="Monkey.Stat" %>
<asp:Content runat="server" ContentPlaceHolderID="Head">
  
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="Body">
 <div id="Stat">
   <fieldset>
   <legend>Solgt</legend>
   <ul class="infoList">
     <li>
       <label>Antal:</label>
       <div class="field"><asp:label runat="server" ID="LblSoldCount"></asp:label></div>
     </li>
     <li>
       <label>Kr:</label>
       <div class="field"><asp:label runat="server" ID="LblSoldKr"></asp:label></div>
     </li>
     <li>
       <label>Købt kr:</label>
       <div class="field"><asp:label runat="server" ID="LblPurchasedKr"></asp:label></div>
     </li>
   </ul>
   </fieldset>
   <fieldset>
   <legend>Rest</legend>
   <ul class="infoList">
     <li>
       <label>Antal:</label>
       <div class="field"><asp:label runat="server" ID="LblRemainCount"></asp:label></div>
     </li>

     <li>
       <label>Købt kr:</label>
       <div class="field"><asp:label runat="server" ID="LblRemainKr"></asp:label></div>
     </li>
   </ul>
   </fieldset>
   <fieldset>
     <legend>Besøg</legend>
     <asp:GridView runat="server" ID="GvVisit" AutoGenerateColumns="False">
       <Columns>
         <asp:BoundField DataField="ip" />
         <asp:BoundField DataField="Timestamp" DataFormatString="{0:dd.MM.yyyy HH:mm:ss}" />
       </Columns>
     </asp:GridView>
   </fieldset>
 </div> 
</asp:Content>

