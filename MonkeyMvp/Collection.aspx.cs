﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using Monkey.Domain;
using Monkey.Infrastructure;
using Monkey.Interfaces;

namespace Monkey
{
	public partial class Collection : System.Web.UI.Page, IFigureView
	{

	  private FigurePresenter<IFigureView> _presenter;

		private IList<Figure>  _figures;


    private bool IsAdmin { get { return Request.QueryString["Admin"] != null; } }

		protected override void OnInit(EventArgs e)
		{
      var dbPath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["DataPath"]);
      _presenter = new FigurePresenter<IFigureView>(this, 
        new FigureRepository(XDocument.Load(dbPath + "\\Collection.xml"), new FigureFactory()), 
        new CollectionFilter(),
        new FileService(), 
        new PathMapper());

			uc1ShowImages.AddImage += OnAddImage;
			uc1ShowImages.ImageFiles = new StringCollection();

			base.OnInit(e);

		}


		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

      _presenter.Onload(! Page.IsPostBack);

		
		}


    public IList<Figure> Figures
    {
      set
      {
        _figures = value;

        uc1ShowImages.ImageFiles = _presenter.MapImages(_figures);
      }
    }




    #region Private

    private string OnAddImage(string filePath)
    {
      string alt = filePath;

      if (!String.IsNullOrEmpty(filePath))
      {
        string prefix = filePath.Split('.')[0];
        int id = Convert.ToInt32(prefix.Substring(prefix.Length - 4, 4));
        alt = String.Format("Abe {0}<br />", id);

        var figure = (from Figure f in _figures where f.Id == id select f).First();

        if (figure != null)
        {
          if (!string.IsNullOrEmpty(figure.Remark))
          {
            alt += Environment.NewLine + String.Format("{0} <br />", figure.Remark);
          }

          if (figure.Height != null)
          {
            alt += Environment.NewLine + String.Format("H*B*D: {0}*{1}*{2} mm. <br />", figure.Height, figure.Width, figure.Length);
          }

          if (!String.IsNullOrEmpty(figure.Material))
          {
            alt += Environment.NewLine + String.Format("Materiale: {0} <br /> ", figure.Material);
          }

          if (IsAdmin)
          {
            alt += Environment.NewLine + String.Format("Pris: {0} kr. <br /> ", figure.Price);
          }
        }
      }
      return alt;
    }

    #endregion
  }
}
