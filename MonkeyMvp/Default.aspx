﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="Default.Master" CodeBehind="Default.aspx.cs" Inherits="Monkey.Default" %>
<%@ Register TagPrefix="uc1" TagName="ShowImages" Src="ShowImages.ascx" %>
<asp:Content runat="server" ContentPlaceHolderID="Head">
  <title></title>

 </asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="Body">
    <div>
			 <fieldset>
			   <legend>Kriterier</legend>
         <ul class="fieldList">
           <li>
             Vælg kriterier og tryk på søg.
           </li>
           <li>
             <label>Nummer:</label>
             <div class="field">
             <asp:DropDownList runat="server" ID="cboId">
						 <asp:ListItem Text="Ikke valgt" Value="" />
						 <asp:ListItem Text="000-199" Value="0" />           
						 <asp:ListItem Text="200-399" Value="200" />           
						 <asp:ListItem Text="400-599" Value="400" />           
						 <asp:ListItem Text="600-799" Value="600" />           
						 <asp:ListItem Text="800-999" Value="800" />           						 
					   </asp:DropDownList>
             </div>
           </li>
           <li>
             <label>Materiale:</label>
             <div class="field">
               <asp:DropDownList runat="server" ID="cboMaterial">
						     <asp:ListItem Text="Ikke valgt" Value="" />
					    </asp:DropDownList>
             </div>
           </li>
           <li>
             <label>Pris:</label>
             <div class="field">
               <asp:DropDownList runat="server" ID="cboPrice">
						 <asp:ListItem Text="Ikke valgt" Value="" />
						 <asp:ListItem Text="000-099" Value="0" />           
						 <asp:ListItem Text="100-199" Value="100" />           
						 <asp:ListItem Text="200-299" Value="200" />           
						 <asp:ListItem Text="300-399" Value="300" />           
						 <asp:ListItem Text="400-499" Value="400" />           
						 <asp:ListItem Text="500-599" Value="500" />                      
						 <asp:ListItem Text="600-699" Value="600" />                                                       
						 <asp:ListItem Text="700-799" Value="700" />           
						 <asp:ListItem Text="800-899" Value="800" />                      
						 <asp:ListItem Text="900-999" Value="900" />           
						 <asp:ListItem Text="1000-1099" Value="1000" />                      
						 <asp:ListItem Text="1100-1199" Value="1100" />                      
					</asp:DropDownList>
             </div>
           </li>
           <li>
             <label>Beskrivelse:</label>
             <div class="field"><asp:TextBox  runat="server" ID="tbRemark" /></div>
           </li>
           <li class="buttons">
            <asp:Button runat="server" ID="btnSearch" Text="Søg" onclick="btnSearch_Click" /> 
           </li>
         </ul>
			 </fieldset>
			 <fieldset>
			   <legend>Aber</legend>
  			  Klik på billede for at se større version. Når større version vises, kan der bladres i billeder med pile-taster.			   
  			  <br />
  			  <br />
         <uc1:ShowImages ID="uc1ShowImages" runat="server" />
			 </fieldset>

    </div>
</asp:Content>
 
