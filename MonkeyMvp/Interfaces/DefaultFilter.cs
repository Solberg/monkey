﻿using System.Collections.Generic;
using System.Linq;
using Monkey.Domain;

namespace Monkey.Interfaces
{
  public class DefaultFilter : IFilterService<Figure>
  {

    private readonly IDefaultView _view;

    public DefaultFilter(IDefaultView view)
    {
      _view = view;
    }

    public IList<Figure> Filter(IList<Figure> items)
    {
      return (from Figure figure in items
		    where (figure.ForSale 
              && figure.SoldFor == null
              && ( _view.Id == null || (figure.Id >= _view.Id && figure.Id <= _view.Id + 199)))
		          && (_view.Material == null || figure.Material == _view.Material)
              && (_view.SalesPrice == null || figure.SalesPrice >= _view.SalesPrice && figure.SalesPrice <= _view.SalesPrice + 99)
              && (string.IsNullOrEmpty(_view.Remark) || figure.Remark.Contains(_view.Remark))
		    select figure).ToList();
    }
  }
}