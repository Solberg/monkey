﻿using System.Linq;
using Monkey.Domain;

namespace Monkey.Interfaces
{
  public class DefaultPresenter : FigurePresenter<IDefaultView>
  {

    public DefaultPresenter(IDefaultView view, 
      IFigureRepository repository, 
      IFilterService<Figure> filterService,
      IFileService fileService,
      IPathMapper pathMapper) : base(view, repository, filterService, fileService, pathMapper)
    {
  
    }

    public override void Onload(bool initializing)
    {
      base.Onload(initializing);

      if (initializing)
      {
        View.Materials = (from Figure figure in FigureRepository.List() where !string.IsNullOrEmpty(figure.Material) select figure.Material).Distinct().ToList();
      }
    }
  }
}