﻿using System.Collections.Generic;
using Monkey.Domain;

namespace Monkey.Interfaces
{
  public interface IFigureView
  {

    IList<Figure> Figures { set; }

  }
}