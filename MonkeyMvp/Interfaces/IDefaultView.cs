﻿using System.Collections.Generic;

namespace Monkey.Interfaces
{
  public interface IDefaultView : IFigureView
  {
    int? Id { get; }

    string Material { get; }

    decimal? SalesPrice { get; }

    string Remark { get; }

    List<string> Materials { set; }

  }
}