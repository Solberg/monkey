﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using Monkey.Domain;

namespace Monkey.Interfaces
{
  public class FigurePresenter<T> where T : class, IFigureView
  {

    private readonly T _view;

    private readonly IFigureRepository _repository;

    private readonly IFilterService<Figure> _filterService;

    private readonly IFileService _fileService;

    private readonly IPathMapper _pathMapper;
    
    protected IFigureRepository FigureRepository { get { return _repository; } }
    
    protected T View { get { return _view; } }

    public FigurePresenter(T view, 
      IFigureRepository repository, 
      IFilterService<Figure> filterService,
      IFileService fileService,
      IPathMapper pathMapper)
    {
      _view = view;
      _repository = repository;
      _filterService = filterService;
      _fileService = fileService;
      _pathMapper = pathMapper;
    }

    public virtual void Onload(bool initializing)
    {
      if (initializing)
      {
        Filter();
      }
    }

    public void Filter()
    {
      _view.Figures = _filterService.Filter(_repository.List());
    }


    public StringCollection MapImages(IEnumerable<Figure> figures)
    {
      var images = new StringCollection();

      foreach (Figure figure in figures)
      {
        string fileName = string.Format("/images/Monkey{0}.jpg", figure.Id.ToString(CultureInfo.InvariantCulture).PadLeft(4, '0'));

        if (_fileService.Exists(_pathMapper.Map(fileName)))
        {
          images.Add(fileName);     
        }
      }

      return images;
    }
  }
}