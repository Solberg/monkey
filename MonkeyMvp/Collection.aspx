﻿<%@ Page Language="C#" MasterPageFile="Default.Master" AutoEventWireup="true" CodeBehind="Collection.aspx.cs" Inherits="Monkey.Collection" %>
<%@ Register TagPrefix="uc1" TagName="ShowImages" Src="ShowImages.ascx" %>

<asp:Content runat="server" ContentPlaceHolderID="Head">
 
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="Body">
    <div>
			 <fieldset>
			   <legend>Aber</legend>
  			  Klik på billede for at se større version. Når større version vises, kan der bladres i billeder med pile-taster.			   
  			  <br />
  			  <br />
         <uc1:ShowImages ID="uc1ShowImages" runat="server" Height="780px" />
			 </fieldset>
      </div>
</asp:Content>
