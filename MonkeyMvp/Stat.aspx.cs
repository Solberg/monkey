﻿using System;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using Monkey.Domain;
using Monkey.Infrastructure;

namespace Monkey
{
	public partial class Stat : System.Web.UI.Page
	{
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
      var dbPath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["DataPath"]);

      var figures = new FigureRepository(XDocument.Load(dbPath + "\\Monkey.xml"), new FigureFactory()).List();

		  var sold = figures.Where(c => c.SoldFor != null && c.SoldFor > 0).ToList();
      var remain = figures.Where(c => (c.SoldFor == null || c.SoldFor == 0) && c.ForSale).ToList();

      decimal iSum = sold.Sum(c => c.SoldFor != null ? (decimal)c.SoldFor : 0);
      decimal iPrice = sold.Sum(c => c.Price != null ? (decimal)c.Price : 0);
			decimal iCnt = sold.Count();


      decimal iRemainCnt = remain.Count();
      decimal iRemainKr = remain.Sum(c => c.Price != null ? (decimal)c.Price : 0);
  
      LblSoldCount.Text = iCnt.ToString(CultureInfo.CurrentCulture);
		  LblSoldKr.Text = iSum.ToString(CultureInfo.CurrentCulture);
		  LblPurchasedKr.Text = iPrice.ToString(CultureInfo.CurrentCulture);
      LblRemainCount.Text = iRemainCnt.ToString(CultureInfo.CurrentCulture);
      LblRemainKr.Text = iRemainKr.ToString(CultureInfo.CurrentCulture);

      var filePath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["Monkey.VisitFile"]);
      var xdoc = XDocument.Load(filePath);
		  GvVisit.DataSource = new VisitRepository(xdoc).List().OrderByDescending(c => c.Timestamp);
      GvVisit.DataBind();

		


		}
		
	}
}
