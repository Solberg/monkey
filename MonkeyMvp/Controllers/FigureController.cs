﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Monkey.Domain;

namespace Monkey.Controllers
{
  public class FigureController : ApiController
  {

    private IFigureRepository repository;

    public FigureController(IFigureRepository repository)
    {
      this.repository = repository;
    }

    // GET api/<controller>
    public IEnumerable<Figure> Get()
    {
      return repository.List();
      //return new string[] { "value1", "value2" };
    }

    // GET api/<controller>/5
    public string Get(int id)
    {
      
      return "value";
    }

    // POST api/<controller>
    public void Post([FromBody]string value)
    {
    }

    // PUT api/<controller>/5
    public void Put(int id, [FromBody]string value)
    {
    }

    // DELETE api/<controller>/5
    public void Delete(int id)
    {
    }
  }
}