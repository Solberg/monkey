﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using Monkey.Domain;
using Monkey.Infrastructure;
using Monkey.Interfaces;

namespace Monkey
{
	public partial class Default : System.Web.UI.Page, IDefaultView
	{
	  private DefaultPresenter _presenter;

	  private IList<Figure> _figures;

		private string OnAddImage(string filePath)
		{
			string alt = filePath;

			if (! String.IsNullOrEmpty(filePath))
			{
				string prefix = filePath.Split('.')[0];
				int id = Convert.ToInt32(prefix.Substring(prefix.Length - 4, 4));
				alt = String.Format("Abe {0}<br />", id );

        var figure = (from Figure f in Figures where f.Id == id select f).First();

				if (figure != null)
				{
					if (!string.IsNullOrEmpty(figure.Remark))
					{
						alt += Environment.NewLine + String.Format("{0} <br />", figure.Remark);
					}

					if (figure.Height != null)
					{
            alt += Environment.NewLine + String.Format("H*B*D: {0}*{1}*{2} mm. <br />", figure.Height, figure.Width, figure.Length);
					}

					if (!string.IsNullOrEmpty(figure.Material))
					{
						alt += Environment.NewLine + String.Format("Materiale: {0} <br /> ", figure.Material);
					}

					if (figure.SalesPrice != null)
					{
						alt += Environment.NewLine + String.Format("Pris: {0} kr. <br /> ", figure.SalesPrice);
					}

				}
			}
		  return alt;
		}

		protected override void OnInit(EventArgs e)
		{
      var dbPath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["DataPath"]);
      _presenter = new DefaultPresenter(this, 
        new FigureRepository(XDocument.Load(dbPath + "\\Monkey.xml"), new FigureFactory()), 
        new DefaultFilter(this),
        new FileService(), 
        new PathMapper());
			uc1ShowImages.AddImage += OnAddImage;
			uc1ShowImages.ImageFiles = new StringCollection();

	    base.OnInit(e);
		}


		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

      _presenter.Onload(! Page.IsPostBack);

			if (! Page.IsPostBack)
			{
        var excludeList = ConfigurationManager.AppSettings["Monkey.ExcludeIp"].Split(';').ToList();

        if (excludeList.Find(c => c == Request.UserHostAddress) == null)
        {
           var filePath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["Monkey.VisitFile"]);
            var xdoc = XDocument.Load(filePath);

          new VisitRepository(xdoc).Create(new Visit(Request.UserHostName, DateTime.Now));
          xdoc.Save(filePath);
        }

        if (Request.QueryString["Remark"] != string.Empty)
        {
          tbRemark.Text = Request.QueryString["Remark"];
          _presenter.Filter();
        }

        if (Request.QueryString["Material"] != string.Empty)
        {
          cboMaterial.SelectedValue = Request.QueryString["Material"];
          _presenter.Filter();
        }

      }
		}



		protected void btnSearch_Click(object sender, EventArgs e)
		{
	    _presenter.Filter();
	  }

    #region IDefaultView implementation

    public int? Id
    {
      get { return cboId.SelectedIndex > 0 ? Convert.ToInt32(cboId.SelectedValue) : (int?)null; }
    }

    public string Material
    {
      get { return cboMaterial.SelectedIndex > 0 ? cboMaterial.Text : null; }
    }

    public decimal? SalesPrice
    {
      get { return cboPrice.SelectedIndex > 0 ? Convert.ToInt32(cboPrice.SelectedValue) : (int?)null; }
    }

    public string Remark
    {
      get { return tbRemark.Text; }
    }

	  public IList<Figure> Figures
	  {
	    private get { return _figures; }
	    set
	    {
	      _figures = value;
		    uc1ShowImages.ImageFiles = _presenter.MapImages(_figures);
	    }
	  }

    #endregion

  


    public List<string> Materials
    {
      set
      {
        cboMaterial.DataSource = value;
        cboMaterial.DataBind();
        cboMaterial.Items.Insert(0, new ListItem("Ikke valgt", "Ikke valgt"));
      }
    }
  }
}
