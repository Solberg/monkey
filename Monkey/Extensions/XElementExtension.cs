﻿using System;
using System.Globalization;
using System.Xml.Linq;

namespace Monkey.Extensions
{
  public static class XElementExtension
  {
    public static int? ToNullableInt(this XElement element)
    {
      return element == null || string.IsNullOrEmpty(element.Value) ? (int?)null : Convert.ToInt32(element.Value);
    }

    public static decimal? ToNullableDecimal(this XElement element)
    {
      return element == null || string.IsNullOrEmpty(element.Value) ? (decimal?)null : Convert.ToDecimal(element.Value.Replace('.', ','));
    }
    public static int ToInt(this XElement element)
    {
      return element == null || string.IsNullOrEmpty(element.Value) ? 0 : Convert.ToInt32(element.Value);
    }

    public static string ToStr(this XElement element)
    {
      return element == null ? null : element.Value;
    }

    public static DateTime? ToNullableDateTime(this XElement element)
    {
      if (element == null || string.IsNullOrEmpty(element.Value))
      {
        return null;
      }

      DateTime dateTime;

      if (DateTime.TryParseExact(element.Value.Substring(0,10), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None,
        out dateTime))
      {
        return dateTime;
      }

      return null;
    }
  }
}