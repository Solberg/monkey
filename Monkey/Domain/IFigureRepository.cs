﻿using System.Collections.Generic;

namespace Monkey.Domain
{
  public interface IFigureRepository
  {
    IEnumerable<Figure> List();

    Figure Fetch(int id);

    /// <summary>
    /// Delete figure from respository
    /// </summary>
    /// <param name="id"></param>
    bool Delete(int id);

    void Update(Figure figure);

    int Create(Figure figure);

  }
}