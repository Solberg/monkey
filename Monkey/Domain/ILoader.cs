﻿namespace Monkey.Domain
{
  public interface ILoader<in TFromType, out TReturnType>
  {
    TReturnType Load(TFromType from);
  }
}