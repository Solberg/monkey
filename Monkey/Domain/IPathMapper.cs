﻿namespace Monkey.Domain
{
  public interface IPathMapper
  {
    string Map(string path);
  }
}