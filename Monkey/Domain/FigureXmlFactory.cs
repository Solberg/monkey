﻿using System.Xml.Linq;

namespace Monkey.Domain
{
  public class FigureXmlFactory : IFactory<Figure, XElement>
  {
    public XElement Create(Figure figure)
    {
      return new XElement("Monkey",
        new XElement("RecNo", figure.Id),
        new XElement("Remark", figure.Remark),
        new XElement("Material", figure.Material),
        new XElement("BoughtAt", figure.BoughtAt),
        new XElement("Material", figure.Material),
        new XElement("Height", figure.Height),
        new XElement("Length", figure.Length),
        new XElement("Width", figure.Width),
        new XElement("BoughtDate", figure.BoughtDate),
        new XElement("Price", figure.Price),
        new XElement("SalesPrice", figure.SalesPrice),
        new XElement("Sold", figure.SoldFor),
        new XElement("ImageName", figure.ImageName  )
        );
    }
  }
}
