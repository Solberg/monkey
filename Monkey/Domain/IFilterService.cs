﻿using System.Collections.Generic;

namespace Monkey.Domain
{
  public interface IFilterService<T> where T : class
  {
    IList<T> Filter(IEnumerable<T> items);
  }
}