﻿using System.Collections.Generic;

namespace Monkey.Domain
{
  public interface IVisitRepository
  {
    void Create(Visit visit);

    List<Visit> List();

  }
}