﻿using System.Xml.Linq;
using Monkey.Extensions;

namespace Monkey.Domain
{

  /// <summary>
  /// Factory which creates figure instanses.
  /// </summary>
  public class FigureFactory : IFactory<XElement, Figure>
  {

    /// <summary>
    /// Create figure from xml.
    /// </summary>
    /// <param name="element"></param>
    /// <returns></returns>
    public Figure Create(XElement element)
    {
      if (element == null)
      {
        return null;
      }

      return new Figure(element.Element("RecNo").ToInt(),
        element.Element("Remark").ToStr(),
        element.Element("Material").ToStr(),
        element.Element("BoughtAt").ToStr(),
        element.Element("Height").ToNullableInt(),
        element.Element("Length").ToNullableInt(),
        element.Element("Width").ToNullableInt(),
        element.Element("BoughtDate").ToNullableDateTime(),
        element.Element("Price").ToNullableDecimal(),

        element.Element("Sold").ToNullableDecimal(),
        element.Element("Sold").ToNullableDecimal() != 0,
        element.Element("SalesPrice").ToNullableDecimal(),
        element.Element("ImageName") != null ? element.Element("ImageName").ToStr() : string.Format($"Monkey{element.Element("RecNo").ToStr().PadLeft(4, '0')}.jpg"));

    }
  }
}
