﻿namespace Monkey.Domain
{

  /// <summary>
  /// Marker class for collection
  /// </summary>
  public interface ICollectionRepository : IFigureRepository
  {
     
  }


}