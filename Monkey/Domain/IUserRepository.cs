﻿using System.Collections.Generic;

namespace Monkey.Domain
{
  public interface IUserRepository
  {
    IList<User> List();
  }
}