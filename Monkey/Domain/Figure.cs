﻿using System;

namespace Monkey.Domain
{
  /// <summary>
  /// Monkey figure
  /// </summary>
  public class Figure
  {

     #region Properties

    /// <summary>
    /// Unique identifier for figure
    /// </summary>
    public int Id { get; set; }
        
    public string Remark { get; set; }

    public string Material { get; set; }

    
    public string BoughtAt { get; set; }

    
    public int? Height { get; set; }

    
    public int? Length { get; set; }

    
    public int? Width { get; set; }

    
    public decimal? Price { get; set; }

    
    public decimal? SoldFor { get; set; }

    
    public decimal? SalesPrice { get; set; }

    
    public bool ForSale { get; set; }

    public string ImageName { get; set; }

    public DateTime? BoughtDate { get; set; }
        #endregion

    
        public Figure(int id, string remark, string material, string boughtAt, int? height, int? length, int? width, DateTime? boughtDate, decimal? price, decimal? soldFor, bool forSale, decimal? salesPrice, string imageName)
        {
          Remark = remark;
          SalesPrice = salesPrice;
          ForSale = forSale;
          SoldFor = soldFor;
          Price = price;
          Width = width;
          Length = length;
          Height = height;
          BoughtDate = boughtDate;
          BoughtAt = boughtAt;
          Material = material;
          Id = id;
          ImageName = imageName;
        }

    public void Update(Figure figure)
    {
      Id = figure.Id;
      ImageName = figure.ImageName;
      BoughtAt = figure.BoughtAt;
      BoughtDate = figure.BoughtDate;
      ForSale = figure.ForSale;
      Height = figure.Height;
      Length = figure.Length;
      Material = figure.Material;
      Price = figure.Price;
      Remark = figure.Remark;
      SalesPrice = figure.SalesPrice;
      SoldFor = figure.SoldFor;
      Width = figure.Width;

    }

  }
}