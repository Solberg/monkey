﻿namespace Monkey.Domain
{
  public interface IPersister<in TType>
  {
    void Persist(TType data);
  }
}