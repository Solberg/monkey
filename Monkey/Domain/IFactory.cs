﻿namespace Monkey.Domain
{
    public interface IFactory<in TFromType, out TToType>
    {

        TToType Create(TFromType element);
    }
}