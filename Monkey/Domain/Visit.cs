﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Monkey.Domain
{
  public class Visit
  {
    public Visit(string ip, DateTime? timestamp)
    {
      Timestamp = timestamp;
      Ip = ip;
    }

    public string Ip { get; private set; }

    public DateTime? Timestamp { get; private set; }


 

  }
}