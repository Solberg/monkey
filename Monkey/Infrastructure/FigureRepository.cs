﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Instrumentation;
using System.Xml.Linq;
using Monkey.Domain;

namespace Monkey.Infrastructure
{
	public class FigureRepository : ICollectionRepository
  {

    #region Variables
    
    /// <summary>
    /// Path to xml file with figures
    /// </summary>
	  private readonly string _dataPath;

    /// <summary>
    /// Xml file loader
    /// </summary>
	  private readonly ILoader<string, XDocument> _dataLoader; 

    /// <summary>
    /// Factory which can generate Figure objects
    /// </summary>
	  private readonly IFactory<XElement, Figure> _figureFactory;

	  private readonly IPersister<IList<Figure>> _persister;

    /// <summary>
    /// Value holder for Figures
    /// </summary>
	  private IList<Figure> _figures;

	  readonly object _lockObject = new object();

    #endregion

    #region Properties

    /// <summary>
    /// Figures in repository
    /// </summary>
    private IList<Figure> Figures
	  {
	    get
	    {
	      lock (_lockObject)
	      {
          var xdoc = _dataLoader.Load(_dataPath);
          return _figures ?? (_figures = (from XElement element in xdoc.Descendants("Monkey") select _figureFactory.Create(element)).ToList());
        }
      }
	  }

    #endregion

    #region Constructors

    public FigureRepository(IFactory<XElement, Figure> figureFactory, IPersister<IList<Figure>> persister, ILoader<string, XDocument> dataLoader, string dataPath)
	  {
	    _figureFactory = figureFactory;
      _persister = persister;
      _dataLoader = dataLoader;
      _dataPath = dataPath;
	  }

    #endregion

    #region IFigureRepository 

    public IEnumerable<Figure> List()
		{

		  return Figures;

    }

	  public Figure Fetch(int id)
	  {
       return (from figure in Figures where figure.Id == id select	figure).FirstOrDefault();
	  }

    /// <summary>
    /// Delete a figure from repository
    /// </summary>
    /// <param name="id"></param>
	  public bool Delete(int id)
    {

      var figure = Fetch(id);

      if (figure == null || ! Figures.Remove(figure))
      {
        return false;
      } 

      lock (_lockObject)
	    {
        _persister.Persist(Figures);
      }

      return true;

	  }

    /// <summary>
    /// Update a existing figure
    /// </summary>
    /// <param name="figure"></param>
	  public void Update(Figure figure)
	  {
      if (figure == null)
      {
        throw new ArgumentNullException(nameof(figure));
      }

	    var item = Figures.Single(c => c.Id == figure.Id);

      if (item == null)
      {
        throw new InstanceNotFoundException($"Figure with id {figure.Id} not found.");
      }

      item.Update(figure);
   
      lock (_lockObject)
      {
        _persister.Persist(Figures);
      }

	  }

    /// <summary>
    /// Create a new figure.
    /// </summary>
    /// <param name="figure"></param>
    /// <returns></returns>
	  public int Create(Figure figure)
	  {
      if (figure == null)
      {
        throw new ArgumentNullException(nameof(figure));
      }

      var id = Figures.Max(c => c.Id);

	    figure.Id = id + 1;

      Figures.Add(figure);

      lock (_lockObject)
      {
        _persister.Persist(Figures);
      }
      
	    return figure.Id;
	  }

	  #endregion

  }
}
