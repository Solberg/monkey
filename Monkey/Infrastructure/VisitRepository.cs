﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using Monkey.Domain;
using Monkey.Extensions;
using Monkey.Interfaces;

namespace Monkey.Infrastructure
{
	public class VisitRepository : IVisitRepository
  {
    #region Variables

    private readonly XDocument _doc;

    #endregion

    public VisitRepository(XDocument doc)
	  {
	    _doc = doc;
	  }


	
    public void Create(Visit visit)
    {
      _doc.Descendants("Visits").First().Add(new XElement("Visit",
              new XElement("Ip", visit.Ip),
              new XElement("Timestamp", visit.Timestamp == null ? string.Empty : ((DateTime)visit.Timestamp).ToString("yyyyMMdd HH:mm:ss"))));

    }

    public List<Visit> List()
    {
      return (from XElement element in _doc.Descendants("Visit")
        select new Visit(element.Element("Ip").ToStr(), element.Element("Timestamp").ToNullableDateTime())).ToList();
    }
  }
}
