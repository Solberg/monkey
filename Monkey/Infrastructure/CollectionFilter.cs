﻿using System.Collections.Generic;
using System.Linq;
using Monkey.Domain;

namespace Monkey.Interfaces
{
  public class CollectionFilter : IFilterService<Figure>
  {
    
    public IList<Figure> Filter(IEnumerable<Figure> items)
    {
      return items.Where(c => c.ForSale == false).ToList();
    }

  }
}