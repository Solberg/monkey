﻿using System;
using System.Xml.Linq;
using Monkey.Domain;

namespace Monkey.Infrastructure
{
  public class XmlLoader : ILoader<string, XDocument>
  {
    public XDocument Load(string @from)
    {
      return XDocument.Load(from);
    }
  }
}
