﻿using System.Collections.Generic;
using System.Linq;
using Monkey.Domain;

namespace Monkey.Infrastructure
{
  public class ForSaleFilter : IFilterService<Figure>
  {
    public IList<Figure> Filter(IEnumerable<Figure> items)
    {
      return (from item in items where item.ForSale && item.SoldFor == null select item).ToList();
    }
  }
}
