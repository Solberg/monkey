﻿using System.Collections.Generic;

namespace MonkeyMvc.Models
{
    public class MonkeyModel
    {
        public List<FigureModel> Figures { get; set; }
    }
}