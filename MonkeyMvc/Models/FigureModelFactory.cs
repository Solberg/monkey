﻿using Monkey.Domain;

namespace MonkeyMvc.Models
{
  public class FigureModelFactory : IFactory<Figure, FigureModel>
  {
    public FigureModel Create(Figure figure)
    {
      if (figure == null)
      {
        return null;
      }

      return new FigureModel()
      {
        BoughtAt = figure.BoughtAt,
        ForSale = figure.ForSale,
        Height = figure.Height,
        Id = figure.Id,
        ImageName = figure.ImageName,
        Length = figure.Length,
        Material = figure.Material,
        Price = figure.Price,
        Remark = figure.Remark,
        SalesPrice = figure.SalesPrice,
        SoldFor = figure.SoldFor,
        Width = figure.Width,
        BoughtDate = figure.BoughtDate
      };
    }
  }
}