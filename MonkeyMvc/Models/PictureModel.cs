﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MonkeyMvc.Models
{
  public class PictureModel
  {

    public int Index { get; set; }

    public string Url { get; set; }

    public string Alt { get; set; }

    public string Active { get; set; }

    public string Description { get; set; }

    public PictureModel()
    { }

    public PictureModel(int index, string url, string alt, string description, string active = null)
    {
      Index = index;
      Url = url;
      Alt = alt;
      Description = description;
      Active = active;

    }

  }
}