﻿using System.Collections.Generic;
using System.Xml.Linq;
using Monkey.Domain;

namespace MonkeyMvc.Infrastructure
{
  public class XmlPersister : IPersister<IList<Figure>>
  {

    private readonly IFactory<Figure, XElement> _figureFactory;

    private readonly string _path;

    public XmlPersister(IFactory<Figure, XElement> figureFactory, string path)
    {
      _figureFactory = figureFactory;
      _path = path;
    }

    public void Persist(IList<Figure> figures)
    {
      var data = new XElement("Monkeys");

      foreach (var figure in figures)
      {
        data.Add(_figureFactory.Create(figure));
      }

      var xdoc = new XDocument();
      xdoc.Declaration = new XDeclaration("1.0", "iso-8859-1", "true");

      xdoc.AddFirst(data);

      xdoc.Save(_path);
    }
  }
}