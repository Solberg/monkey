﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Monkey.Domain;
using MonkeyMvc.Domain;
using MonkeyMvc.Models;

namespace MonkeyMvc.Infrastructure
{
  public class RandomPictureFactory : IRandomPictureFactory
  {

    private readonly IFigureRepository _figureRepository;

    private readonly IPathMapper _pathMapper;

    private readonly IFilterService<Figure> filterService; 

    public RandomPictureFactory(IFigureRepository figureRepository, IPathMapper pathMapper, IFilterService<Figure> filterService)
    {
      _figureRepository = figureRepository;
      _pathMapper = pathMapper;
      this.filterService = filterService;
    }


    public List<PictureModel> Create(int numberOfEntries)
    {
      var figures = filterService.Filter(_figureRepository.List());

      var pictures = new List<PictureModel>();

      Random ran = new Random(int.Parse(Guid.NewGuid().ToString().Substring(0, 8), System.Globalization.NumberStyles.HexNumber));

      int count = 0;

      while (pictures.Count < numberOfEntries)
      {
        var idx = ran.Next(1, figures.Count());

        var fileName = "/images/figures/" + figures[idx].ImageName;

        if (File.Exists(_pathMapper.Map(fileName)))
        {
          pictures.Add(new PictureModel(count, fileName, $"Figur nummer {figures[idx].Id}", figures[idx].Remark, count == 0 ? "active" : ""));
          count ++;
        }
      }
     
      return pictures;
    }
  }
}