﻿using System.Web;

namespace MonkeyMvc.Domain
{
  public interface IFileUploader
  {

    string Upload(HttpPostedFileBase file);

  }
}