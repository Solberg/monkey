﻿using System.Collections.Generic;
using MonkeyMvc.Models;

namespace MonkeyMvc.Domain
{
  public interface IRandomPictureFactory
  {
    List<PictureModel> Create(int numberOfEntries);
  }
}