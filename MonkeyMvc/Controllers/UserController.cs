﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Monkey.Domain;
using MonkeyMvc.Models;

namespace MonkeyMvc.Controllers
{
    public class UserController : Controller
    {

      private IUserRepository userRepository;

      public UserController(IUserRepository userRepository)
      {
        this.userRepository = userRepository;
      }

      // GET: User
        public ActionResult Index()
        {
            return View();
        }

      [HttpGet]
      public ActionResult LogIn()
      {
        return View();
      }

      public ActionResult LogOut()
      {
        FormsAuthentication.SignOut();
        return RedirectToAction("Index", "Home");
      }

    [HttpPost]
    public ActionResult LogIn(UserModel user)
    {
      if (ModelState.IsValid)
      {
        if (IsValid(user.Email, user.Password))
        {
          FormsAuthentication.SetAuthCookie(user.Email, false);
          return RedirectToAction("Index", "Home");
        }
        ModelState.AddModelError("Email", "Login data is incorrect");
      }
      return View(user);
    }


      private bool IsValid(string email, string password)
      {
        
        bool isValid = false;

        var user = userRepository.List().FirstOrDefault(c => c.Email == email);

        if (user != null)
        {
          isValid = user.Password == password;
        }

        return isValid;
        
      }

  }
}