﻿using System.IO;
using System.Linq;
using System.Web.Mvc;
using Monkey.Domain;
using MonkeyMvc.Domain;
using MonkeyMvc.Models;

namespace MonkeyMvc.Controllers
{

  [Authorize]
  public class CollectionController : Controller
    {

      private readonly ICollectionRepository _collectionRepository;

      private readonly IFactory<Figure, FigureModel> _figureModelFactory;

      private readonly IFactory<FigureModel, Figure> _figureFactory;

    private readonly IFileUploader _fileUploader;

      public CollectionController(ICollectionRepository collectionRepository, IFactory<Figure, FigureModel> figureModelFactory, IFactory<FigureModel, Figure> figureFactory, IFileUploader fileUploader)
      {
        _collectionRepository = collectionRepository;
        _figureModelFactory = figureModelFactory;
        _figureFactory = figureFactory;
        _fileUploader = fileUploader;
      }


  // GET: Collection
    public ActionResult Index()
    {
      var model = (from figure in _collectionRepository.List() select _figureModelFactory.Create(figure)).ToList();
      return View(model);
    }

    [HttpGet]
    public ActionResult Details(int id)
    {
      var model = _collectionRepository.Fetch(id);
      return PartialView(_figureModelFactory.Create(model));
    }
    [HttpGet]
    public ActionResult Edit(int id)
    {
      var figure = _collectionRepository.Fetch(id);

      return View(_figureModelFactory.Create(figure));
    }

    [HttpPost]
    public ActionResult Edit(FigureModel model)
    {
      if (ModelState.IsValid)
      {
        try
        {
            var newImage = _fileUploader.Upload(model.File);
            model.ImageName = string.IsNullOrEmpty(newImage) ? model.ImageName : newImage;
        }
        catch (IOException exception)
        {
          ModelState.AddModelError("File", exception.Message);
        }

        if (ModelState.IsValid)
        {
          _collectionRepository.Update(_figureFactory.Create(model));

          return RedirectToAction("Index");
        }
      }

      return View(model);
    }


    [HttpGet]
      public ActionResult Create()
      {
      return View(new FigureModel());
    }

    [HttpPost]
    public ActionResult Create(FigureModel model)
    {

      if (ModelState.IsValid)
      {
        try
        {
          var newImage = _fileUploader.Upload(model.File);
          model.ImageName = string.IsNullOrEmpty(newImage) ? model.ImageName : newImage;
        }
        catch (IOException exception)
        {
          ModelState.AddModelError("File", exception.Message);
        }
   
        if (ModelState.IsValid)
        {
          _collectionRepository.Create(_figureFactory.Create(model));

          return RedirectToAction("Index");
        }
      }
      return View(model);
    }

    public ActionResult Delete(int id)
    {
      if (id == 0)
      {
        return RedirectToAction("Index");
      }

      _collectionRepository.Delete(id);

      return RedirectToAction("Index");
    }
  

  }
}