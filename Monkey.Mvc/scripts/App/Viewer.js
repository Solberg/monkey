﻿var Viewer = {};

Viewer.getMonkey = function(id) {
    var url = $('#modalDetails').data('url');

    url += "/" + id;

    $.get(url, function(data) {
        $('.modal-body').html(data);

        $('#modalDetails').modal('show');
    });
};