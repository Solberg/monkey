﻿using System.Web.Mvc;
using Monkey.Mvc.Domain;

namespace Monkey.Mvc.Controllers
{
  public class HomeController : Controller
  {

    private readonly IRandomPictureFactory _randomPictureFactory;

    public HomeController(IRandomPictureFactory randomPictureFactory)
    {
      _randomPictureFactory = randomPictureFactory;
    }


    public ActionResult Index()
    {
      var model = _randomPictureFactory.Create(10);
      return View(model);
    }
  }
}
