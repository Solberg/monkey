﻿using Monkey.Domain;
using Monkey.Mvc.Models;
using System.Linq;
using System.Web.Mvc;

namespace Monkey.Mvc.Controllers
{
  [Authorize]
  public class StatisticsController : Controller
    {

      public StatisticsController(IFigureRepository collectionRepository)
     {
        this.figureRepository = collectionRepository;
     }


    private IFigureRepository figureRepository;

        // GET: Statistics
        public ActionResult Index()
        {
           var figuresForSale = figureRepository.List().Where(c => c.SoldFor != 0);

           var figuresSold = figuresForSale.Where(c => c.SoldFor != null && c.SoldFor != 0).ToList();

            var model = new StatisticsModel() { TotalCount = figuresForSale.Count(),
              TotalAmount = figuresForSale.Where(c => c.Price != null).Sum(c => (decimal)c.Price),
              SoldCount = figuresSold.Count(),
              SoldAmount = figuresSold.Where(c => c.SoldFor != null).Sum(c => (decimal)c.SoldFor),
              SoldPurchasePrice = figuresSold.Where(c => c.Price != null).Sum(c => (decimal)c.Price),
              RemainCount = figuresForSale.Count() - figuresSold.Count()
            };

            return View(model);
        }
    }
}