﻿using System.IO;
using Monkey.Domain;
using Monkey.Mvc.Models;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using Monkey.Mvc.Domain;


namespace Monkey.Mvc.Controllers
{
  public class MonkeyController : Controller
  {


     private readonly IFigureRepository _figureRepository;

     private readonly IFactory<Figure, FigureModel>_figureModelFactory;

    private readonly IFactory<FigureModel, Figure> _figureFactory; 

    private readonly IFilterService<Figure> _filterService;

    private readonly IFileUploader _fileUploader;


    public MonkeyController(IFigureRepository figureRepository, IFactory<Figure, FigureModel> figureModelFactory, IFilterService<Figure> filterService, IFactory<FigureModel, Figure> figureFactory, IFileUploader fileUploader)
    {
      _figureRepository = figureRepository;
      _figureModelFactory = figureModelFactory;
      _filterService = filterService;
      _figureFactory = figureFactory;
      _fileUploader = fileUploader;
    }

    [System.Web.Mvc.HttpGet]
    public ActionResult Edit(int? id)
    {
      if (id == null)
      {
        return RedirectToAction("Index");
      }

      var model = (from figure in _figureRepository.List() where figure.Id==id select _figureModelFactory.Create(figure)).FirstOrDefault();
      return View(model);

    }

    public ActionResult Delete(int? id)
    {
      if (id == null)
      {
        return RedirectToAction("Index");
      }

      _figureRepository.Delete((int)id);
      return RedirectToAction("Index");
    }


    [System.Web.Mvc.HttpPost]
    public ActionResult Edit(FigureModel model)
    {
      if (ModelState.IsValid)
      {
        try
        {
          var newImage = _fileUploader.Upload(model.File);
          model.ImageName = string.IsNullOrEmpty(newImage) ? model.ImageName : newImage;
        }
        catch (IOException exception)
        {
          ModelState.AddModelError("File", exception.Message);
        }



        _figureRepository.Update(_figureFactory.Create(model));
        return RedirectToAction("Index");
      }
      
      return View(model);
    }

   


    [System.Web.Mvc.HttpPost]
    public ActionResult Search(string searchTerm)
    {
      var model = _filterService.Filter(_figureRepository.List()).Where(c => c.Remark.ToLower().Contains(searchTerm.ToLower())).Select(c => _figureModelFactory.Create(c));
      return View(model);
    }

    [System.Web.Mvc.HttpGet]
    public ActionResult Search([FromUri]string searchTerm, string dummy)
    {
      var model = (from figure in _filterService.Filter(_figureRepository.List())
                   where string.IsNullOrEmpty(searchTerm) 
                   || figure.Remark.ToLower().Contains(searchTerm.ToLower())
                   || (figure.Material != null && figure.Material.ToLower().Contains(searchTerm.ToLower()))
                   select _figureModelFactory.Create(figure)).ToList();
      return View(model);
    }
    public ActionResult Index()
    {
            var model = new MonkeyModel()
            {
                Figures = (from figure in _filterService.Filter(_figureRepository.List())
                           select _figureModelFactory.Create(figure)).ToList()
            } ;

      return View(model);
    }

    [System.Web.Mvc.HttpGet]
    public ActionResult Details(int id)
    {
      var model = _figureRepository.Fetch(id); 
      return PartialView(_figureModelFactory.Create(model));
    }

  }
}
