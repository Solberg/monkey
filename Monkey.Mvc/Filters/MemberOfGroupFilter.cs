﻿using System.Web.Mvc;
using Monkey.Mvc.Domain;

namespace Monkey.Mvc.Filters
{


  public class MemberOfGroupFilter : ActionFilterAttribute
  {

    /// <summary>
    /// Property injected by IOC container
    /// </summary>
    public ISecurityService SecurityService { get; set; }

    private string _group;

    public MemberOfGroupFilter(string group)
    {
      _group = group;
    }

    public override void OnActionExecuting(ActionExecutingContext filterContext)
    {
      base.OnActionExecuting(filterContext);

      SecurityService.AccessAllowed(_group);
    }
  }
}