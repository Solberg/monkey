﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Monkey.Domain;

namespace Monkey.Mvc.Models
{
  public class FigureFactory : IFactory<FigureModel, Figure>
  {
    public Figure Create(FigureModel model)
    {
      if (model == null)
      {
        return null;
      }

      return new Figure(model.Id, model.Remark, model.Material, model.BoughtAt, model.Height, model.Length, model.Width, model.BoughtDate, model.Price, model.SoldFor, model.ForSale, model.SalesPrice, model.ImageName);
    }

  }
}