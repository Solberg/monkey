﻿using System.ComponentModel.DataAnnotations;

namespace Monkey.Mvc.Models
{
  public class StatisticsModel
  {

    [Display(Name = "Antal figurer solgt")]
    public int SoldCount { get; set; }

    [Display(Name = "Total solgt")]
    public decimal SoldAmount { get; set; }

    [Display(Name = "Købspris for solgte")]
    public decimal SoldPurchasePrice { get; set; }

    [Display(Name = "Antal figurer ialt")]
    public int TotalCount { get; set; }

    [Display(Name = "Total køb")]
    public decimal TotalAmount { get; set; }

    [Display(Name = "Resterende figurer")]
    public int RemainCount { get; set; }

  }
}