﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Monkey.Mvc.Models
{

  /// <summary>
  /// Monkey figure
  /// </summary>
  public class FigureModel
  {
    #region Properties

    /// <summary>
    /// Unique identifier for figure.
    /// </summary>
    /// <remarks>
    /// No remarks
    /// </remarks>
    public int Id { get; set; }

    /// <summary>
    /// Description off the figure.
    /// </summary>
    [Display(Name = "Bemærkning")]
    [DataType(DataType.MultilineText)]
    [Required(ErrorMessage = "Bemærkning skal udfyldes")]
    public string Remark { get; set; }

    /// <summary>
    /// Which material is the figure made off
    /// </summary>
    [Display(Name = "Materiale")]
    public string Material { get; set; }

    /// <summary>
    /// Place of purchase
    /// </summary>
    [Display(Name = "Købssted")]
    [Required(ErrorMessage = "Købssted skal udfyldes")]
    public string BoughtAt { get; set; }

    [Required(ErrorMessage = "Købsdato skal udfyldes")]
    [Display(Name="Købsdato")]
    [DataType(DataType.Date)]
    [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
    public DateTime? BoughtDate { get; set; }

    /// <summary>
    /// Height of the figure
    /// </summary>
    [Display(Name = "Højde")]
    public int? Height { get; set; }

    /// <summary>
    /// Length of the figure.
    /// </summary>
    [Display(Name = "Længde")]
    public int? Length { get; set; }

    /// <summary>
    /// Width of the figure
    /// </summary>
    [Display(Name = "Bredde")]
    public int? Width { get; set; }

    /// <summary>
    /// Price when purchased
    /// </summary>
    [Required(ErrorMessage = "Pris skal udfyldes")]
    [Display(Name = "Pris")]
    [DataType(DataType.Currency)]
    [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}")]
    public decimal? Price { get; set; }

    [Display(Name = "Solgt for")]
    [DataType(DataType.Currency)]
    [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}")]
    public decimal? SoldFor { get; set; }

    [Display(Name = "Salgspris")]
    [DataType(DataType.Currency)]
    [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:c}")]
    public decimal? SalesPrice { get; set; }

    [Display(Name = "Til salg")]
    public bool ForSale { get; set; }

    public string ImageName { get; set; }

    public HttpPostedFileBase File { get; set; }

    #endregion

  }
}