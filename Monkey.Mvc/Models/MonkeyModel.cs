﻿using System.Collections.Generic;

namespace Monkey.Mvc.Models
{
    public class MonkeyModel
    {
        public List<FigureModel> Figures { get; set; }
    }
}