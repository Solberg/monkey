﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Monkey.Mvc.Models
{
  public class ActionModel
  {

    public string Action { get; set; }
    
    public string Controller { get; set; }

  }
}