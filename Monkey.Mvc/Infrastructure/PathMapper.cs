﻿using System.Web;
using Monkey.Domain;

namespace Monkey.Mvc.Infrastructure
{
  public class PathMapper : IPathMapper
  {
    public string Map(string path)
    {
       return HttpContext.Current.Server.MapPath(path);
    }
  }
}