﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Monkey.Domain;
using Monkey.Mvc.Domain;
using Monkey.Mvc.Models;
using SSL.Io;

namespace Monkey.Mvc.Infrastructure
{
  public class RandomPictureFactory : IRandomPictureFactory
  {

    private readonly IFigureRepository _figureRepository;

    private readonly IPathMapper _pathMapper;

    private readonly IFilterService<Figure> filterService;

    private IFileService fileService;

    public RandomPictureFactory(IFigureRepository figureRepository, 
      IPathMapper pathMapper, 
      IFilterService<Figure> filterService,
      IFileService fileService)
    {
      _figureRepository = figureRepository;
      _pathMapper = pathMapper;
      this.filterService = filterService;
      this.fileService = fileService;
    }


    public List<PictureModel> Create(int numberOfEntries)
    {
      var figurePath = _pathMapper.Map("/Images/Figures");

      var figures = filterService.Filter(_figureRepository.List());

      var pictures = new List<PictureModel>();

      if (fileService.ListFiles(figurePath).Count() < numberOfEntries)
      {
        return pictures;
      }

      Random ran = new Random(int.Parse(Guid.NewGuid().ToString().Substring(0, 8), System.Globalization.NumberStyles.HexNumber));

      int count = 0;
      int notFound = 0;
      while (pictures.Count < numberOfEntries && notFound < 100)
      {
        var idx = ran.Next(1, figures.Count());

        var fileName = "/images/figures/" + figures[idx].ImageName;

        if (fileService.Exists(_pathMapper.Map(fileName)))
        {
          pictures.Add(new PictureModel(count, fileName, $"Figur nummer {figures[idx].Id}", figures[idx].Remark, figures[idx].SalesPrice, count == 0 ? "active" : ""));
          count ++;
        }
        else
        {
          notFound++;
        }
      }
     
      return pictures;
    }
  }
}