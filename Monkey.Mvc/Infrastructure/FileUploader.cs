﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web;
using Monkey.Domain;
using Monkey.Mvc.Domain;

namespace Monkey.Mvc.Infrastructure
{
  public class FileUploader : IFileUploader
  {

    private readonly IPathMapper _pathMapper;

    public FileUploader(IPathMapper pathMapper)
    {
      _pathMapper = pathMapper;
    }

    public string Upload(HttpPostedFileBase file)
    {
      if (file != null && file.ContentLength > 0 && ! string.IsNullOrEmpty(file.FileName))
      {
        try
        {
          string fileName = Path.GetFileName(file.FileName);

          string path = Path.Combine(_pathMapper.Map("~/Images/Figures"), fileName);
          file.SaveAs(path);

          var image = Image.FromFile(path);

          var newImage = new Bitmap(200, 150);

          using (var graphics = Graphics.FromImage(newImage))
          {
            graphics.DrawImage(image, 0, 0, 200, 150);
          }

          newImage.Save(Path.GetDirectoryName(path) + @"\index\" + fileName, ImageFormat.Jpeg);

          return fileName;
        }
        catch (Exception ex)
        {
          throw new IOException($"Error uploading file {file.FileName}: {ex.Message}");
        }
      }
      return null;
    }
  }
}