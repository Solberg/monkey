﻿namespace Monkey.Mvc.Domain
{
  public interface ISecurityService
  {

    bool AccessAllowed(string group);

  }
}