﻿using System.Web;

namespace Monkey.Mvc.Domain
{
  public interface IFileUploader
  {

    string Upload(HttpPostedFileBase file);

  }
}