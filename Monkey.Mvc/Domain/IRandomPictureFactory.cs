﻿using System.Collections.Generic;
using Monkey.Mvc.Models;

namespace Monkey.Mvc.Domain
{
  public interface IRandomPictureFactory
  {
    List<PictureModel> Create(int numberOfEntries);
  }
}