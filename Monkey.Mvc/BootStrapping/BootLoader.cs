﻿using System.Collections.Generic;
using System.Reflection;
using System.Web.Http;
using Autofac;
using System.Xml.Linq;
using Monkey.Mvc.Domain;
using Monkey.Domain;
using Monkey.Infrastructure;
using Monkey.Mvc.Infrastructure;
using Monkey.Mvc.Models;
using Ssl.Ioc.Autofac;
using SSL.Domain;
using SSL.Io;

namespace Monkey.Mvc.BootStrapping
{
  public class BootLoader : IBootLoader
  {
    public void Boot()
    {
      var salesFile = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/Monkey.xml");
      var collectionFile = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/Collection.xml");

      var builder = new ContainerBuilder();

      var mvcModule = new MvcModule();
      var webApiModule = new WebApiModule(GlobalConfiguration.Configuration);

      mvcModule.Configure(builder, Assembly.GetExecutingAssembly());
      webApiModule.Configure(builder, Assembly.GetExecutingAssembly());

      builder.RegisterType<FileService>().As<IFileService>();
      builder.RegisterType<PathMapper>().As<IPathMapper>();
      builder.RegisterType<UserRepository>().As<IUserRepository>();
      builder.RegisterType<ForSaleFilter>().As<IFilterService<Figure>>();
      builder.RegisterType<Monkey.Domain.FigureFactory>().As<IFactory<XElement, Figure>>();
      builder.RegisterType<Models.FigureFactory>().As<IFactory<FigureModel, Figure>>();
      builder.RegisterType<FigureModelFactory>().As<IFactory<Figure, FigureModel>>();
      builder.RegisterType<FigureXmlFactory>().As<IFactory<Figure, XElement>>();
      builder.RegisterType<XmlPersister>().As<IPersister<IList<Figure>>>();
      builder.RegisterType<XmlLoader>().As<ILoader<string, XDocument>>();
      builder.RegisterType<FileUploader>().As<IFileUploader>();

      builder.Register(c => new FigureRepository(c.Resolve<IFactory<XElement, Figure>>(), new XmlPersister(c.Resolve<IFactory<Figure, XElement>>(), c.Resolve<IFileService>(), salesFile), c.Resolve<ILoader<string, XDocument>>(), salesFile)).As<IFigureRepository>();
      builder.Register(c => new FigureRepository(c.Resolve<IFactory<XElement, Figure>>(), new XmlPersister(c.Resolve<IFactory<Figure, XElement>>(), c.Resolve<IFileService>(), collectionFile), c.Resolve<ILoader<string, XDocument>>(), collectionFile)).As<ICollectionRepository>();
      builder.RegisterType<RandomPictureFactory>().As<IRandomPictureFactory>();

      // Set the dependency resolver to be Autofac.
      var container = builder.Build();

      webApiModule.SetResolver(container);
      mvcModule.SetResolver(container);
    }

    public void Dispose()
    {
    }
  }
}