﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monkey.Domain;
using Monkey.Mvc.Infrastructure;
using NUnit.Framework;
using SSL.IO;

namespace Monkey.Tests.Mvc.Infrastructure
{

  [TestFixture]
  public class XmlPersisterTests
  {

    [Test]
    public void persist_success()
    {
      var figures = new List<Figure>()
      {
        new Figure(1, "Remark", "Material", "BoughtAt", 1,2,3, DateTime.Now, 1000, 1100, false, 2000, "Monkey0001.jpg"),
        new Figure(2, "Remark", "Material", "BoughtAt", 1,2,3, DateTime.Now, 1000, 1100, false, 2000, "Monkey0001.jpg")

      };
      
      new XmlPersister(new FigureXmlFactory(), new SSL.Io.FileService(), "c:\\temp\\test.xml").Persist(figures);

    }

  }
}
