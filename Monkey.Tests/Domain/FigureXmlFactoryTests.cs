﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monkey.Domain;
using NUnit.Framework;

namespace Monkey.Tests.Domain
{

  /// <summary>
  /// Tests of factory which creat xml for a figure.
  /// </summary>
  [TestFixture]
  public class FigureXmlFactoryTests
  {

    [Test]
    public void create_success()
    {
      var result = new FigureXmlFactory().Create(new Figure(1, "Remark", "Material", "BoughtAt", 100, 200, 300, DateTime.Now, 1000, 1100, true, 1200, "Monkey0001.jpg"));

      Assert.IsNotNull(result);

    }

  }
}
