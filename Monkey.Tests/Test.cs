﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Monkey.Tests
{

  [TestFixture]
  public class Test
  {

    [Test]
    public void test_method()
    {
      IEnumerable<string> updates;

      List<string> list;

      List<string> readOnly = new List<string>();


      var repository = new Repository();

      updates = repository.ListReadOnly();

      list = repository.List().ToList();

      readOnly.AddRange(repository.List());
      
      repository.Add("test");

      Assert.AreEqual(1, updates.Count());
      Assert.AreEqual(0, list.Count);
      Assert.AreEqual(0, readOnly.Count);


    }


}

  public class Repository
  {
    private List<string> items = new List<string>();

    public void Add(string s)
    {
      items.Add(s);
    }

    public IEnumerable<string> ListReadOnly()
    {
      
      return items;
      
    }

    public IEnumerable<string> List()
    {
      return items;
    } 




  }

}
