﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Monkey.Domain;
using Monkey.Infrastructure;
using Moq;
using NUnit.Framework;

namespace Monkey.Tests.Infrastructure
{

  [TestFixture]
  public class FigureRepositoryTests
  {

    /// <summary>
    /// Should be able to list data
    /// </summary>
    [Test]
    public void be_able_to_list()
    {
      var dataLoaderMock = new Mock<ILoader<string, XDocument>>();
      var persisterMock = new Mock<IPersister<IList<Figure>>>();

      const string xml = "<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>"
                         + "<NewDataSet>"
                         + "  <Monkey>"
                         + "    <RecNo>1</RecNo>"
                         + "    <Remark>Mrk. Amphora. Mrk. 827327.</Remark>"
                         + "    <Material>Porcelæn</Material>"
                         + "    <BoughtAt>Runners Antik Studiestræde København</BoughtAt>"
                         + "    <Picturequality>Digitalt</Picturequality>"
                         + "    <Height>240</Height>"
                         + "    <Length>160</Length>"
                         + "    <Width>150</Width>"
                         + "    <BoughtDate>1989-03-04T00:00:00+01:00</BoughtDate>"
                         + "    <Price>1000</Price>"
                         + "    <Sold>0</Sold>"
                         + "  </Monkey>"
                         + "  <Monkey>"
                         + "    <RecNo>2</RecNo>"
                         + "    <Remark>Kgl. porc. Nr.20222. Snærrer. Siddende. Grå/brun. Kappebavian. Drejer hovedet. Kunstner : Kuhn.</Remark>"
                         + "    <Material>Stentøj</Material>"
                         + "    <BoughtAt>Axelved</BoughtAt>"
                         + "    <Picturequality>Digitalt</Picturequality>"
                         + "    <Height>63</Height>"
                         + "    <Length>47</Length>"
                         + "    <Width>32</Width>"
                         + "    <Weight>61</Weight>"
                         + "    <BoughtDate>1989-01-01T00:00:00+01:00</BoughtDate>"
                         + "    <Price>40</Price>"
                         + "    <Sold>0</Sold>"
                         + "  </Monkey>"
                         + "</NewDataSet>";

      dataLoaderMock.Setup(c => c.Load(It.IsAny<string>())).Returns(XDocument.Parse(xml));

      var result = new FigureRepository(new FigureFactory(), persisterMock.Object, dataLoaderMock.Object, string.Empty).List();

      var figures = result.ToList();

      Assert.IsNotNull(result);
      Assert.IsTrue(figures.Count == 2);
      var figure = figures[0];

      Assert.IsTrue(figure.Id == 1);
      Assert.IsTrue(figure.Remark == "Mrk. Amphora. Mrk. 827327.");
      Assert.IsTrue(figure.Material == "Porcelæn");
      Assert.IsTrue(figure.Height == 240);
      Assert.IsTrue(figure.Length == 160);
      Assert.IsTrue(figure.Width == 150);
      Assert.IsTrue(figure.Price == 1000);
      Assert.IsFalse(figure.ForSale);
      Assert.IsTrue(figure.SalesPrice == null);
      
    }

  }
}
