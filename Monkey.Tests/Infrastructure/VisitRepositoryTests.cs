﻿using System;
using System.Linq;
using System.Xml.Linq;
using Monkey.Domain;
using Monkey.Infrastructure;
using NUnit.Framework;

namespace Monkey.Tests.Infrastructure
{
  /// <summary>
  /// Summary description for VisitProviderTests
  /// </summary>
  [TestFixture]
  public class VisitRepositoryTests
  {

    [Test]
    public void be_able_to_create()
    {
      var doc = XDocument.Parse("<Visits></Visits>");
      new VisitRepository(doc).Create(new Visit("127.0.0.1", DateTime.Now));

      Assert.IsTrue(doc.Descendants("Visit").Count() == 1);

    }

    [Test]
    public void be_able_to_list()
    {
      var doc = XDocument.Parse("<Visits><Visit><Ip>127.0.0.1</Ip><Timestamp>20131231 22:22:00</Timestamp></Visit></Visits>");
      var result = new VisitRepository(doc).List();

      Assert.IsNotNull(result);
      Assert.IsTrue(result.Count == 1);
      Assert.IsTrue(result[0].Ip == "127.0.0.1");
      Assert.IsTrue(result[0].Timestamp == new DateTime(2013,12,31,22,22,0));
    }

  }
}
